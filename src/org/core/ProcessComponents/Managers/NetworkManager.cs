﻿using Infrastructure.Manager.Network;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ui.Environment.UserDomain;

namespace ProcessComponents.Managers
{
    public class NetworkManager : INetworkManager
    {
        private HubConnection hubConnection;

        private EnteredUser enteredUser;

        public HubConnection HubConnection
        {
            get
            {
                 return hubConnection;
            }
        }

        public NetworkManager()
        {
            hubConnection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                await hubConnection.StartAsync();
            };
        }

        public void AdminConnection()
        {
            if (hubConnection != null)
            {
                this.StopConnection();
                hubConnection = null;
            }

            if (hubConnection == null)
            {
                hubConnection = new HubConnectionBuilder()
                .WithUrl("http://localhost:5000/adminHub")
                .Build();
                this.StartConnection();
            }
        }

        public void DefaultConnection()
        {
            if (hubConnection != null)
            {
                this.StopConnection();
                hubConnection = null;
            }

            if (hubConnection == null)
            {
                hubConnection = new HubConnectionBuilder()
                .WithUrl("http://localhost:5000/openHub")
                .Build();
                this.StartConnection();
                hubConnection.InvokeAsync("BackConnectedAsync", enteredUser.Username);
            }
        }

        private async void StartConnection()
        {
            try
            {
                await hubConnection?.StartAsync();
                hubConnection.On<string, string, string, string>("SomeMethod", ());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private async void StopConnection()
        {
            try
            {
                await hubConnection?.StopAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
