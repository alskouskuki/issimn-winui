﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Manager.Network
{
    public interface INetworkManager
    {
        HubConnection HubConnection { get; }

        void DefaultConnection();

        void AdminConnection();
    }
}
